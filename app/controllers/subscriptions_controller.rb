class SubscriptionsController < InheritedResources::Base
  def new

    @user = current_user
    @plan = Plan.all
    @subscription = nil
    if current_user.subscriptions.present?
      @subscription = current_user.subscriptions.order('updated_at desc').first
    end
  end

  def create
    @subscription = Subscription.new(params[:subscription])
    if @subscription.save_with_payment
      redirect_to @subscription, :notice => "Thank you for subscribing!"
    else
      render :new
    end
  end

  def show
    @subscription = Subscription.find(params[:id])
  end

  def change_plan
    if current_user.credit_card.blank? || current_user.account.blank?
      respond_to do |format|
        format.json { render json: {:status => :failure} }
      end
    else
      plan = Plan.find(params["plan_id"])
      sub = current_user.subscriptions
      if sub.blank?
        Subscription.create({:plan_id => params["plan_id"].to_i, :user_id => current_user.id})

        Stripe::Charge.create(
            :amount => plan.price,
            :currency => "usd",
            :customer => current_user.credit_card.stripe_customer_id
        )
      else
        sub_active = sub.where({:plan_id => plan.id}).first
        if (sub_active.blank?)
            Subscription.create({:plan_id => params["plan_id"].to_i, :user_id => current_user.id})
            Stripe::Charge.create(
                :amount => plan.price,
                :currency => "usd",
                :customer => current_user.credit_card.stripe_customer_id
            )
        else
          sub_active.update_attributes({:updated_at => Time.now})
        end

      end
      respond_to do |format|
        format.json { render json: {:status => :ok} }
      end
    end

  end
end
